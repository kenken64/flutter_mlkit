import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

void main(){
  runApp(SmartGallery());
}

class SmartGallery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Smart Photo Gallery',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SmartGalleryPage(title : 'Smart Photo Gallery'),
    );
  }
}

class SmartGalleryPage extends StatefulWidget {
  SmartGalleryPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _SmartGalleryPageState createState() => _SmartGalleryPageState();
}

class SinglePhoto {
  String text;
  String path;

  SinglePhoto(this.text, this.path);

  @override
  String toString() {
    return '{ ${this.text}, ${this.path} }';
  }
}


class _SmartGalleryPageState extends State<SmartGalleryPage> {
  List<SinglePhoto> storedImagesPath = [];
  File _imageFile;
  String _mlResult = '<no result>';
  final _picker= ImagePicker();

  Future<void> _imageLabelling() async {
    setState(() => this._mlResult = '<no result>');
    if (await _pickImage() == false) {
      return;
    }
    String result = '';
    final FirebaseVisionImage visionImage =
    FirebaseVisionImage.fromFile(this._imageFile);
    final ImageLabeler labelDetector = FirebaseVision.instance.imageLabeler();
    final List<ImageLabel> labels =
    await labelDetector.processImage(visionImage);
    result += 'Detected ${labels.length} labels.\n';
    for (final ImageLabel label in labels) {
      final String text = label.text;
      final String entityId = label.entityId;
      final double confidence = label.confidence;
      result +=
      '\n#Label: $text($entityId), confidence=${confidence.toStringAsFixed(3)}';
    }
    if (result.isNotEmpty) {
      setState(() {
        print(result);
        this._mlResult = result;
        storedImagesPath.add(SinglePhoto(result, this._imageFile.path));
      });
    }
  }


  Future<bool> _pickImage() async {
    setState(() => this._imageFile = null);
    final File imageFile = await showDialog<File>(
      context: context,
      builder: (ctx) => SimpleDialog(
        children: <Widget>[
          ListTile(
            leading: const Icon(Icons.camera_alt),
            title: const Text('Take picture'),
            onTap: () async {
              final PickedFile pickedFile =
              await _picker.getImage(source: ImageSource.camera);
              Navigator.pop(ctx, File(pickedFile.path));
            },
          ),
          ListTile(
            leading: const Icon(Icons.image),
            title: const Text('Pick from gallery'),
            onTap: () async {
              try {
                final PickedFile pickedFile =
                await _picker.getImage(source: ImageSource.gallery);
                Navigator.pop(ctx, File(pickedFile.path));
              } catch (e) {
                print(e);
                Navigator.pop(ctx, null);
              }
            },
          ),
        ],
      ),
    );
    if (imageFile == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Please pick one image first.')),
      );
      return false;
    }
    setState(() => this._imageFile = imageFile);
    print('picked image: ${this._imageFile}');
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              color: Colors.black,
              icon: Icon(Icons.camera_enhance_sharp, size: 30.0),
              onPressed: this._imageLabelling,
            ),
          ]
      ),
      body: Container(
        child: Card(
          elevation: 10,
          child: Container(
            alignment: Alignment.center,
            child: GridView.builder(
              itemCount: storedImagesPath.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1,
                crossAxisSpacing: 10,
                mainAxisSpacing: 8,
              ),
              itemBuilder: (BuildContext context, int index){
                return Container(
                  child: Column(
                    children: [
                      Container(
                        height: 150.0,
                        width: 150.0,
                        child: Image.file(File(storedImagesPath[index].path))
                      ),
                      Text(storedImagesPath[index].text),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

